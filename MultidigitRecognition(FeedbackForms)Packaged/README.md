## QUICK EXECUTION GUIDE :
    - Clone the repository
    - Execute the command 'python main1.py' in the terminal.

## ENTITY WISE GUIDE :
    - "main1.py" is the source code of the project
    - "multidigitdetection model.py" contains the code for CNN model training for predicting 
      MNIST dataset. It is not necessary to perform the training.I have already trained and saved
      the model as cnn_mnist_best.h5 containing the pretrained weights.
    - "FeedbackForms" directory contains feedback form images for the project.
    - "cells" directory contains a directory corresponding to all FeedbackForms Images.
       Its subdirectory contains images of cells of the form image and it further contains 
       directories corresponding to each cell of the image which contain the segmented digits 
       of the multidigit cells.
    - "frames" contains the images with embedded predictions of every cell in the original image.
    - "FFCorrect.csv" contains the correct output of te Feedback Forms.
    - "FFResult.csv" contains the predicted result for the cells of the Feedback Forms images.
    - "output_cnn.csv is the testing sample output prediction for the MNIST model using 
       multidigitdetection model.py CNN model"
    - "IVP Mini Project Report.pdf" is the report for the project.
    - "Gattal-Chibani2011_Chapter_SegmentationStrategyOfHandwrit.pdf" is paper on connected digit 
      segmentation approach published in 2011.
    - "IJCIAJournal.pdf" is paper on same topic published in 2015.
    
## PERFORMANCE :
    MNIST Model used has an accuracy of 99.7% on MNIST dataset.
    Feedback forms multidigit cell are predicted with an accuracy of 92.31%.