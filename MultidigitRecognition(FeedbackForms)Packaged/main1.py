import numpy as np
import pandas
import cv2
import imutils
import os
import csv
import os.path
from os import path
from keras.models import load_model

model = load_model('cnn_mnist_best.h5')

def grid_ext(image):
    """JAR ESIHB
    to extract grid of the given image
    :gray:Grayscaled version of image 
    :thresh:Edge image-->Image after closing
    :kernel:Structuring Element for Closing
    :cnts:List of contour points
    :max_c:Largest contour
    :asp_rat:Aspect ratio of the image
    :return:Extracted grid image
    """
    # Grayscale conversion
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Threshold
    thresh = cv2.Canny(gray, 40, 120)
    # Closing
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 3))
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

    # ROI Detection
    #We are looking for the external contours, which simply corresponds to their outlines.
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 
    cnts = imutils.grab_contours(cnts)                      
    max_c = max(cnts, key=cv2.contourArea)                  # largest contour/ req grid (sorting on the basis of contour area)
    cnt = [list(pt[0]) for pt in max_c]                     # converts into list of contour points

    b_r = max(cnt, key=lambda x: x[0] + x[1])               # b_r = bottom right 
    t_l = min(cnt, key=lambda x: x[0] + x[1])               # t_l = top left
    t_r = max(cnt, key=lambda x: x[0] - x[1])               # t_r = top right
    b_l = min(cnt, key=lambda x: x[0] - x[1])               # b_l = bottom left

    #Adding a padding of 2 so that we don't miss outer boundary lines of the grid.
    b_r[0], b_r[1] = b_r[0] + 2, b_r[1] + 2 
    b_l[0], b_l[1] = b_l[0] - 2, b_l[1] + 2
    t_r[0], t_r[1] = t_r[0] + 2, t_r[1] - 2
    t_l[0], t_l[1] = t_l[0] - 2, t_l[1] - 2

    asp_rat = (b_l[1] - t_l[1]) / (t_r[0] - t_l[0])

    w = 750
    h = int(round(w*asp_rat))

    pts1 = np.float32([t_l, t_r, b_l, b_r])                 # pts1: from shape
    pts2 = np.float32([[0, 0], [h, 0], [0, w], [h, w]])     # pts2: to shape
    morph = cv2.getPerspectiveTransform(pts1, pts2)         # Get perspective transform for transform from pts1 to pts2
    fin_image = cv2.warpPerspective(image, morph, (h, w))
    return fin_image
def rem_multi_lines(lines, thresh):
    """
    to remove the multiple lines with close proximity 
    :param lines: initial list with all the lines(multiple in place of singular)
    :param thresh: dist between two lines for them to be considered as same
    :return: final list with singular lines in place of multiple
    """
    a = []
    i = 0
    lines.append([800, 0])                                  # random val/ noise
    out = []
    # this loop collects lines with close proximity in a list (a) and then appends that 
    # complete list in a common list called out.
    while i < len(lines) - 1:
        if lines[i] not in a:
            a.append(lines[i])
        if abs(lines[i + 1][0] - lines[i][0]) < thresh:
            a.append(lines[i + 1])
        else:
            out.append(a)
            a = []
        i += 1
    
    # print(out)

    final = []
    #Traversing out list to find the average of every bunch of close proximity list in out list and appending it to final
    for i in out:
        a = np.array(i)
        final.append(np.average(a, axis=0))

    for i in final.copy():
        if i[0] < 0:
            final.remove(i)
    return final


def draw_r_theta_lines(img, lines, color):
    """
    draw lines on image which are of (r, theta) form
    :param img: image to draw the lines on
    :param lines: list of lines on the form (r, theta)
    :param color: color of lines
    :return: 
    """
    for rho, theta in lines:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        x1 = int(x0 + 1000 * (-b))
        y1 = int(y0 + 1000 * a)
        x2 = int(x0 - 1000 * (-b))
        y2 = int(y0 - 1000 * a)

        cv2.line(img, (x1, y1), (x2, y2), color, 2)

def lines_ext(img, hough_thresh, multilines_thresh):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)                    
    edges = cv2.Canny(gray, 45, 10)

    line_image = img.copy()

    lines = cv2.HoughLines(edges, 1, np.pi / 180, hough_thresh)
    lines = lines.reshape(lines.shape[0], 2)

    draw_r_theta_lines(line_image, lines, (0, 0, 255))

    lines = sorted(lines, key=lambda x: x[0])           # Sort lines on the basis of their rho value

    l1 = list(lines)
    l2 = []
    for i in l1:
        l2.append(list(i))
    v_lines = []
    h_lines = []

    for i in l2:
        if round(i[1]) == 0:                            # If theta=0  then it is a vertical line
            v_lines.append(i)
        elif round(i[1]) > 0.8:                         #Else horizontal    
            h_lines.append(i)


    v_lines = rem_multi_lines(v_lines, multilines_thresh)
    h_lines = rem_multi_lines(h_lines, multilines_thresh)

    final = v_lines + h_lines

    draw_r_theta_lines(line_image, final, (0, 255, 0))

    return v_lines, h_lines



def intersection_bw_2_lines(l1, l2):
    """
    Returns point of intersection between 2 lines
    Parameters:
        l1 : line1
        l2 : line2
    Returns:
        x and y coordinate of point of intersection of l1 and l2
    """
    rho1, theta1 = l1                           #Extract rho and theta for line1
    rho2, theta2 = l2                           #Extract rho and theta for line2      

    a = np.array([
        [np.cos(theta1), np.sin(theta1)],       #Creating an array of 
        [np.cos(theta2), np.sin(theta2)]
    ])
    b = np.array([[rho1], [rho2]])              #An array of rhos 

    x0, y0 = np.linalg.solve(a, b)
    x0, y0 = int(np.round(x0)), int(np.round(y0))

    return [x0, y0]


def cells_ext(h_lines, v_lines, start_r=0, start_c=0, end_r=0, end_c=0):
    """
    Extracts four corners of the cell from the horizontal and vertical lines POI
    :param h_lines: r, theta form horizontal lines
    :param v_lines: r, theta form vertical lines
    :param start_r: starting row number
    :param start_c: starting column number
    :param end_r: ending row number
    :param end_c: ending column number
    :return: final list containing the four corners of individual cells
    """
    start_r=len(h_lines)-11
    start_c=len(v_lines)-7
    if end_r == end_c == 0:
        end_c = len(v_lines) - 1
        end_r = len(h_lines) - 1
    ret_cell = []
    for i in range(0, len(h_lines) - 1):
        for j in range(0, len(v_lines) - 1):
            hl1, hl2 = h_lines[i], h_lines[i + 1]
            vl1, vl2 = v_lines[j], v_lines[j + 1]

            p1 = intersection_bw_2_lines(hl1, vl1)
            p2 = intersection_bw_2_lines(hl1, vl2)
            p3 = intersection_bw_2_lines(hl2, vl1)
            p4 = intersection_bw_2_lines(hl2, vl2)

            ret_cell.append([p1, p2, p3, p4])

    for i in range(len(ret_cell)):
        p1, p2, p3, p4 = ret_cell[i]
        p1 = (p1[0] + 2, p1[1] + 2)
        p2 = (p2[0] - 2, p2[1] + 2)
        p3 = (p3[0] + 2, p3[1] - 2)
        p4 = (p4[0] - 2, p4[1] - 2)
        ret_cell[i] = p1, p2, p3, p4

    ret_cell_fin = []

   
    for i in range(len(ret_cell)):
        if start_r <= (i / (len(v_lines) - 1)) <= (end_r + 1) and start_c <= (i % (len(v_lines) - 1)) <= end_c:
            ret_cell_fin.append(ret_cell[i])
    return ret_cell_fin
def sort_contours(cnts, method="left-to-right"):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0
    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True
    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1
    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),key=lambda b:b[1][i], reverse=reverse))
    # return the list of sorted contours and bounding boxes
    return (cnts, boundingBoxes)

import random as rng
def pad_with(vector, pad_width, iaxis, kwargs):
    pad_value = kwargs.get('padder', 10)
    vector[:pad_width[0]] = pad_value
    vector[-pad_width[1]:] = pad_value

#Init
if(path.exists("frames")!=True):
    os.mkdir("frames")
if(path.exists("cells")!=True):
    os.mkdir("cells")
img_count=18
correct_count=0
df = pandas.read_csv('FFCorrect.csv')
row_entry=[]

#Iterating through all images 
for q in range(img_count):
    col_entry=[]
    image=cv2.imread("FeedbackForms/img"+str(q)+".jpg",1)
    image = grid_ext(image)
    v_lines, h_lines = lines_ext(image, 240, 10)
    ret_cell_fin = cells_ext(h_lines, v_lines, 0, 0, 0,0)
    cell_image = image.copy()
    frame=image.copy()
    if(path.exists("cells/cells_raj"+str(q))!=True):
        os.mkdir("cells/cells_raj"+str(q))
    
    print(q)
    
    for i in range(len(ret_cell_fin)):
        p1, p2, p3, p4 = ret_cell_fin[i]
        cell = cell_image[p1[1]:p3[1], p1[0]:p2[0]]
        cv2.imwrite( "cells/cells_raj" +str(q)+ "/" + str(i) + ".jpg", cell)
        if(path.exists("cells/cells_raj"+str(q)+"/digits"+str(i))!=True):
            os.mkdir("cells/cells_raj"+str(q)+"/digits"+str(i))
        
    for i in range(len(ret_cell_fin)):
        img=cv2.imread("cells/cells_raj" +str(q)+ "/" + str(i) + ".jpg",0)
        # ret,thresh2 = cv2.threshold(img,150,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        thresh2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,61,43)
        ro,co=thresh2.shape
        thresh2=thresh2[0:ro,2:co]
        kernel = np.ones((2,2),np.uint8)
        kernel1 = np.ones((2,1),np.uint8)
        dilation = cv2.dilate(thresh2,kernel,iterations = 1)
        dilation=cv2.erode(dilation,kernel1,1)
        # dilation = cv2.morphologyEx(dilation, cv2.MORPH_OPEN, kernel)
        _ ,contours, _ = cv2.findContours(dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours_poly = [None]*len(contours)
        boundRect = [None]*len(contours)
        if(len(contours)!=0):
            (contours, boundingBoxes) = sort_contours(contours, method="left-to-right")
        for r, c in enumerate(contours):
            contours_poly[r] = cv2.approxPolyDP(c,0.1, True)
            boundRect[r] = cv2.boundingRect(contours_poly[r])

        string = ""

        for l in range(len(contours)):
            area=boundRect[l][2] * boundRect[l][3]
            if 10 <= boundRect[l][3] <= 50 and 2 <= boundRect[l][2] <= 45 and 20 < area < 1300:
                digit_bound_box=dilation[int(boundRect[l][1]):int(boundRect[l][1]+boundRect[l][3]),int(boundRect[l][0]):int(boundRect[l][0]+boundRect[l][2])]    
                u,v=digit_bound_box.shape
                u=round((28-min(28,u))/2)
                v=round((28-min(28,v))/2)
                np.pad(digit_bound_box, ((u, u), (v, v)),'constant', constant_values=((0,0),(0,0)))
                digit_bound_box=np.pad(digit_bound_box, 10, pad_with, padder=0)
                digit_bound_box=cv2.resize(digit_bound_box,(28,28))
                cv2.imwrite("cells/cells_raj"+str(q)+"/digits"+str(i)+"/"+str(l)+".jpg",digit_bound_box)
                digit_bound_box = digit_bound_box.reshape(1,28,28,1)
                digit_bound_box = digit_bound_box.astype('float32')
                digit_bound_box /= 255
                f=np.argmax(model.predict(digit_bound_box))
                string = string + str(f)
        # print(string)

        num=""
        if df.values[q][i] == df.values[q][i]:
            num=str(int(df.values[q][i]))
            col_entry.append(int(num))
        else:
            col_entry.append(float("nan"))
        
        if num == string:
            correct_count = correct_count+1

        #Embedding predicted cell outputs in the original image frame
        x,y,w,h = ret_cell_fin[i]
        frame = cv2.rectangle(frame, (x[0],x[1]), (y[0],w[1]), (36,255,12), 1)
        cv2.putText(frame, string , (x[0], x[1]+10), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, (0,0,255), 2)

    cv2.imwrite("frames/frame"+str(q)+'.jpg',frame)
    # Appending every row of the predicted output
    row_entry.append(col_entry)
# Reporting the accuracy
acc=(correct_count*100)/(img_count*60)
print("Accuracy = "+ str(acc) + " %")

# Writing all predicted outputs in FFResult.csv
with open('FFResult.csv', 'w') as csvfile:   
    csvwriter = csv.writer(csvfile)  
    csvwriter.writerows(row_entry)

